//
//  MoviesViewModel.swift
//  MazenProject
//
//  Created by Mazen Denden on 9/6/19.
//  Copyright © 2019 Mazen Denden. All rights reserved.
//

import Foundation

class MovieViewModel {
    private var movie = Movie()
    
    init(movie: Movie = Movie()) {
        self.movie = movie
    }
    
    var title: String { return movie.title ?? "" }
    var year: Int { return movie.releaseYear ?? 0 }
    var rating: Float { return movie.rating ?? 0 }
    var runtime: Int { return movie.runtime ?? 0 }
    var summary: String { return movie.summary ?? "" }
    var language: String {
        return movie.language == "English" ? "US" : ""
    }
    var coverImage: String { return movie.coverImage ?? "" }
    var screenshots: [String] { return movie.screenshots }

}
