//
//  MovieScreenshotsTableViewCell.swift
//  MazenProject
//
//  Created by Mazen Denden on 10/24/19.
//  Copyright © 2019 Mazen Denden. All rights reserved.
//

import UIKit

private let imagesCollectionViewCellId = "imagesCollectionViewCellId"

class MovieScreenshotsTableViewCell: UITableViewCell {
    let titleLabel: UILabel = {
        let label = UILabel()
        label.text = "Images"
        label.textColor = .lightGray
        label.textAlignment = .center
        label.font = UIFont.boldSystemFont(ofSize: 14)
        return label
    }()
    
    let imagesCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.backgroundColor = .black
        return collectionView
    }()
    
    var imagesArray: [String] = []
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupCell()
    }
    
    func setupCell() {
        backgroundColor = .black
        selectionStyle = .none
        
        setupCollectionView()
        
        addSubview(titleLabel)
        titleLabel.anchor(topAnchor: (topAnchor, 8),
                          centerXAnchor: (centerXAnchor, 0))
        
        addSubview(imagesCollectionView)
        imagesCollectionView.anchor(topAnchor: (titleLabel.bottomAnchor, 8),
                                    leftAnchor: (leftAnchor, 0),
                                    rightAnchor: (rightAnchor, 0),
                                    bottomAnchor: (bottomAnchor, 8),
                                    height: 200)
    }
    
    func setupCollectionView() {
        imagesCollectionView.delegate = self
        imagesCollectionView.dataSource = self
        
        imagesCollectionView.register(ImagesCollectionViewCell.self, forCellWithReuseIdentifier: imagesCollectionViewCellId)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension MovieScreenshotsTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imagesArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: imagesCollectionViewCellId, for: indexPath) as? ImagesCollectionViewCell else { return UICollectionViewCell() }
        cell.imageUrl = imagesArray[indexPath.item]
        return cell
    }
    
}

extension MovieScreenshotsTableViewCell: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let height = collectionView.frame.height
        return CGSize(width: height, height: height)
    }
}
