//
//  MovieCell.swift
//  MazenProject
//
//  Created by Mazen Denden on 10/14/19.
//  Copyright © 2019 Mazen Denden. All rights reserved.
//

import UIKit
import Kingfisher

class MovieCell: UICollectionViewCell {
    
    var imageType = ImageType.circle
    
    var movieViewModel: MovieViewModel? {
        didSet {
            guard let movieViewModel = movieViewModel else { return }
            guard let imageUrl = URL(string: movieViewModel.coverImage) else { return }
            
            self.setKingFisherImage(withUrl: imageUrl)
        }
    }
    
    let movieImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.layer.masksToBounds = false
        imageView.layer.borderColor = UIColor.white.cgColor
        imageView.layer.borderWidth = 2
        imageView.clipsToBounds = true
        return imageView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = ColorConstants.backgroundBlack
        
        contentView.addSubview(movieImageView)
        
        movieImageView.anchor(topAnchor: (contentView.topAnchor, 6),
                              leftAnchor: (contentView.leftAnchor, 6),
                              bottomAnchor: (contentView.bottomAnchor, 6),
                              width: 100)
        
        DispatchQueue.main.async {
            switch self.imageType {
            case .circle:
                self.movieImageView.layer.cornerRadius = 50
                
            default:
                self.movieImageView.layer.cornerRadius = 15
                self.movieImageView.layer.borderWidth = 0
            }
        }
        
    }
    
    func setKingFisherImage(withUrl url: URL) {
        movieImageView.kf.setImage(
            with: url,
            placeholder: UIImage(named: "placeholderImage"),
            options: [
                .cacheOriginalImage,
                .transition(.fade(1))
            ]
        )
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
