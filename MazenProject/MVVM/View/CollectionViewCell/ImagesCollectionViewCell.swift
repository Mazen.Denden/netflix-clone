//
//  ImagesCollectionViewCell.swift
//  MazenProject
//
//  Created by Mazen Denden on 10/25/19.
//  Copyright © 2019 Mazen Denden. All rights reserved.
//

import UIKit

class ImagesCollectionViewCell: UICollectionViewCell {
    let movieImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = #imageLiteral(resourceName: "nLogo")
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()
    
    var imageUrl: String? {
        didSet {
            guard let url = URL(string: imageUrl ?? "") else { return }
            
            self.setKingFisherImage(withUrl: url)
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupCell()
    }
    
    func setupCell() {
        backgroundColor = .black
        layer.cornerRadius = 30
        clipsToBounds = true
        
        addSubview(movieImageView)
        movieImageView.anchor(topAnchor: (topAnchor, 0),
                              leftAnchor: (leftAnchor, 0),
                              rightAnchor: (rightAnchor, 0),
                              bottomAnchor: (bottomAnchor, 0))
    }
    
    func setKingFisherImage(withUrl url: URL) {
        movieImageView.kf.setImage(
            with: url,
            placeholder: UIImage(named: "placeholderImage"),
            options: [
                .cacheOriginalImage,
                .transition(.fade(1))
            ]
        )
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
