//
//  Loader.swift
//  MazenProject
//
//  Created by Mazen Denden on 10/15/19.
//  Copyright © 2019 Mazen Denden. All rights reserved.
//

import UIKit

class Loader {
    
    static let backgroundView: UIView = {
        let view = UIView()
        view.backgroundColor = ColorConstants.transparentBlackColor
        return view
    }()
    
    static var loader = CustomLoader()
    
    static func showLoader() {
        guard let window = UIApplication.shared.windows.first(where: { $0.isKeyWindow }) else {
            return
        }
        
        window.addSubview(backgroundView)
        backgroundView.anchor(centerXAnchor: (window.centerXAnchor, 0), centerYAnchor: (window.centerYAnchor, 0))
        backgroundView.widthAnchor.constraint(equalTo: window.widthAnchor).isActive = true
        backgroundView.heightAnchor.constraint(equalTo: window.heightAnchor).isActive = true
        
        backgroundView.addSubview(loader)
        loader.anchor(centerXAnchor: (backgroundView.centerXAnchor, 0),
                      centerYAnchor: (backgroundView.centerYAnchor, 0),
                      width: 150, height: 150)
        
        backgroundView.fadeIn()
        loader.startAnimation()
    }
    
    static func hideLoader() {
        backgroundView.fadeOut()
        loader.stopAnimation()
        loader.removeFromSuperview()
    }

}
