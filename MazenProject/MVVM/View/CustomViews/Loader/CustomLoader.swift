//
//  CustomLoader.swift
//  MazenProject
//
//  Created by Mazen Denden on 10/15/19.
//  Copyright © 2019 Mazen Denden. All rights reserved.
//

import UIKit

struct GeoPoint: Equatable {
    let xPoint: CGFloat
    let yPoint: CGFloat
    
    static func == (lhs: GeoPoint, rhs: GeoPoint) -> Bool {
        return
            lhs.xPoint == rhs.xPoint &&
                lhs.yPoint == rhs.yPoint
    }
}

class CustomLoader: UIView {
    var alphaDistance: CGFloat = 0
    var ratio: CGFloat = 0.125
    var circleRadius: CGFloat = 0
    
    let frameWidth: CGFloat = 100
    
    var circlesColor: UIColor = ColorConstants.backgroundRed.withAlphaComponent(0.6)
    var mainCircleColor: UIColor = ColorConstants.backgroundRed
    
    var circles: [GeoPoint] = []
    var filteredCircles: [GeoPoint] = []
    private var animate: Bool = false
    var firstIteration: Bool = true
    
    let mainView = UIView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        layer.cornerRadius = 25
        backgroundColor = ColorConstants.backgroundBlack
        
        circleRadius = frameWidth * ratio
        alphaDistance = ( frameWidth * 0.25 ) / 100
        
        mainView.layer.cornerRadius = ( circleRadius + (10 * alphaDistance) ) / 2
        mainView.backgroundColor = mainCircleColor
        
        fillCirclesArray()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func fillCirclesArray() {
        let topPoint = GeoPoint(xPoint: 0, yPoint: 100 * alphaDistance)
        let topRightPoint = GeoPoint(xPoint: 70.71 * alphaDistance, yPoint: 70.71 * alphaDistance)
        let rightPoint = GeoPoint(xPoint: 100 * alphaDistance, yPoint: 0)
        let bottomRightPoint = GeoPoint(xPoint: 70.71 * alphaDistance, yPoint: -70.71 * alphaDistance)
        let bottomPoint = GeoPoint(xPoint: 0, yPoint: -100 * alphaDistance)
        let bottomLeftPoint = GeoPoint(xPoint: -70.71 * alphaDistance, yPoint: -70.71 * alphaDistance)
        let leftPoint = GeoPoint(xPoint: -100 * alphaDistance, yPoint: 0)
        let topleftPoint = GeoPoint(xPoint: -70.71 * alphaDistance, yPoint: 70.71 * alphaDistance)
        
        circles = [topPoint, topRightPoint, rightPoint, bottomRightPoint, bottomPoint, bottomLeftPoint, leftPoint, topleftPoint]
        filteredCircles = circles
    }
    
    func startAnimation() {
        self.drawMainCircle()
        
        animate = true
        DispatchQueue.main.async {
            self.animateMainCircle(point: self.filteredCircles[0])
        }
    }
    
    func stopAnimation() {
        // Main view's tag
        if let viewWithTag = viewWithTag(101) {
            viewWithTag.removeFromSuperview()
        }
        
        animate = false
        self.subviews.forEach { $0.layer.removeAllAnimations() }
        self.layer.removeAllAnimations()
        self.layoutIfNeeded()
    }
    
    func animateMainCircle(point: GeoPoint) {
        UIView.animate(withDuration: 0.35, animations: {
            self.mainView.transform = CGAffineTransform(translationX: point.xPoint, y: -point.yPoint)
        }, completion: { _ in
            if self.animate {
                self.filteredCircles = self.filteredCircles.filter { $0 != self.filteredCircles[0] }
                if self.filteredCircles.isEmpty {
                    self.filteredCircles = self.circles
                }
                self.animateMainCircle(point: self.filteredCircles[0])
            }
        })
    }
    
    override func draw(_ rect: CGRect) {
        let centerX = self.frame.width / 2 - circleRadius / 2
        let centerY = self.frame.height / 2 - circleRadius / 2
        
        circles.forEach { (circle) in
            let path = UIBezierPath(ovalIn: CGRect(x: centerX + circle.xPoint, y: centerY - circle.yPoint, width: circleRadius, height: circleRadius))
            circlesColor.withAlphaComponent(0.5).setFill()
            path.fill()
        }
    }
    
    func drawMainCircle() {
        mainView.tag = 101
        self.addSubview(mainView)
        let mainViewRadius = circleRadius + (10 * alphaDistance)
        
        mainView.anchor(centerXAnchor: (self.centerXAnchor, 0), centerYAnchor: (self.centerYAnchor, 0), width: mainViewRadius, height: mainViewRadius)
        
        if firstIteration {
            self.mainView.transform = CGAffineTransform(translationX: filteredCircles[0].xPoint, y: -filteredCircles[0].yPoint)
            firstIteration = false
        }
        
    }
}
