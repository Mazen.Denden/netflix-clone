//
//  CustomTextField.swift
//  MazenProject
//
//  Created by Mazen Denden on 10/11/19.
//  Copyright © 2019 Mazen Denden. All rights reserved.
//

import UIKit

@IBDesignable
class CustomTextField: UITextField {
    
    var topMaring: CGFloat = 0
    
    var animationDuration: CGFloat = 0.5
    var animationDelay: Int = 0
    var springWithDamping: CGFloat = 0.7
    var springVelocity: CGFloat = 0
    
    var wrongColor: UIColor = .red
    
    let textLabel = UILabel()
    
    let viewUnderTextField = UIView()
    
    var labelText: String = "" {
        didSet {
            textLabel.text = labelText
        }
    }
    
    var labelColor: UIColor = .white {
        didSet {
            textLabel.textColor = labelColor
        }
    }
    
    var viewColor: UIColor = .white {
        didSet {
            viewUnderTextField.backgroundColor = viewColor
        }
    }
    
    var marginFromFieldToLabel: CGFloat = 0
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        createSubViews()
        self.borderStyle = .none
        self.addTarget(self, action: #selector(animateView), for: .editingDidBegin)
        self.addTarget(self, action: #selector(animateBackView), for: .editingDidEnd)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    fileprivate func createSubViews() {
        self.addSubview(textLabel)
        self.addSubview(viewUnderTextField)
        
        textLabel.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: 0).isActive = true
        textLabel.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 4).isActive = true
        
        viewUnderTextField.topAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        viewUnderTextField.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        viewUnderTextField.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        viewUnderTextField.heightAnchor.constraint(equalToConstant: 1).isActive = true
    }
    
    @objc func animateView() {
        setValid()
        let xPos = -self.textLabel.frame.origin.x
        let yPos = -self.frame.height
        UIView.animate(withDuration: TimeInterval(animationDuration),
                       delay: TimeInterval(animationDelay),
                       usingSpringWithDamping: springWithDamping,
                       initialSpringVelocity: springVelocity,
                       options: .curveEaseOut,
                       animations: {
            self.textLabel.transform = CGAffineTransform(translationX: xPos, y: yPos)
        })
    }
    
    @objc func animateBackView() {
        let text = self.text ?? ""
        if !text.isEmpty {
            return
        }
        
        UIView.animate(withDuration: TimeInterval(animationDuration),
                       delay: TimeInterval(animationDelay),
                       usingSpringWithDamping: springWithDamping,
                       initialSpringVelocity: springVelocity,
                       options: .curveEaseOut,
                       animations: {
            self.textLabel.transform = .identity
        })
    }
    
    func setError() {
        textLabel.shakeAnimation()
        
        self.viewUnderTextField.backgroundColor = wrongColor
        self.textLabel.textColor = wrongColor
    }
    
    func setValid() {
        self.viewUnderTextField.backgroundColor = viewColor
        self.textLabel.textColor = labelColor
    }
}
