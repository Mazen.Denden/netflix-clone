//
//  MovieDetailViewController.swift
//  MazenProject
//
//  Created by Mazen Denden on 10/23/19.
//  Copyright © 2019 Mazen Denden. All rights reserved.
//

import UIKit

let dataCellId = "dataCell"
let descriptionCellId = "descriptionCell"
let screenshotsCellId = "screenshotsCell"

class MovieDetailsViewController: BaseViewController {
    
    let dataTableView: UITableView = {
        let tableView = UITableView()
        tableView.backgroundColor = .black
        tableView.contentInsetAdjustmentBehavior = .never
        tableView.separatorColor = .white
        tableView.separatorInset = UIEdgeInsets(top: 0, left: 50, bottom: 0, right: 50)
        tableView.showsVerticalScrollIndicator = false
        return tableView
    }()

    var movieViewModel = MovieViewModel()
    
    var header: MovieDetailsHeaderView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupTableView()
        setupViews()
    }
    
    func setupTableView() {
        dataTableView.delegate = self
        dataTableView.dataSource = self
        
        dataTableView.register(MovieDataTableViewCell.self, forCellReuseIdentifier: dataCellId)
        dataTableView.register(MovieDescriptionTableViewCell.self, forCellReuseIdentifier: descriptionCellId)
        dataTableView.register(MovieScreenshotsTableViewCell.self, forCellReuseIdentifier: screenshotsCellId)

        dataTableView.tableFooterView = UIView()
    }
    
    func setupViews() {
        backButtonView.itemsColor = .white
        backButtonView.rightItem.text = movieViewModel.title
        
        view.addSubview(dataTableView)
        
        dataTableView.anchor(topAnchor: (view.topAnchor, 0),
                             leftAnchor: (view.leftAnchor, 0),
                             rightAnchor: (view.rightAnchor, 0),
                             bottomAnchor: (view.bottomAnchor, 0))
        
        setupHeader()
        
    }
    
}
