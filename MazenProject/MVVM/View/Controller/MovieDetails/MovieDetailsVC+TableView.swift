//
//  MovieDetailVC+Extensions.swift
//  MazenProject
//
//  Created by Mazen Denden on 10/24/19.
//  Copyright © 2019 Mazen Denden. All rights reserved.
//

import UIKit

enum MovieDetailCellType: CaseIterable {
    case data
    case description
    case screenshots
}

private let headerHeight = UIScreen.main.bounds.height * 0.43

extension MovieDetailsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return MovieDetailCellType.allCases.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: dataCellId, for: indexPath) as? MovieDataTableViewCell else { return UITableViewCell() }
            cell.movieViewModel = movieViewModel
            return cell
        case 1:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: descriptionCellId, for: indexPath) as? MovieDescriptionTableViewCell else { return UITableViewCell() }
            cell.descriptionLabel.text = movieViewModel.summary
            return cell
        default:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: screenshotsCellId, for: indexPath) as? MovieScreenshotsTableViewCell else { return UITableViewCell() }
            cell.imagesArray = movieViewModel.screenshots
            return cell
        }
    }
    
    func setupHeader() {
        header = MovieDetailsHeaderView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: headerHeight))
        header?.setupHeaderImage(imageString: self.movieViewModel.coverImage)
        
        dataTableView.tableHeaderView = nil
        dataTableView.contentInset = UIEdgeInsets(top: headerHeight, left: 0, bottom: 0, right: 0)
        dataTableView.addSubview(header ?? UIView())
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if header != nil {
            let yPos: CGFloat = -scrollView.contentOffset.y
            
            if yPos > 0 {
                var headerRect: CGRect = header?.frame ?? CGRect()
                
                headerRect.origin.y = -yPos
                headerRect.size.height = yPos
                
                header?.frame = headerRect

                self.dataTableView.sectionHeaderHeight = headerRect.size.height
            }
        }
    }
    
}
