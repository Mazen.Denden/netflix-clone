//
//  HomeViewController.swift
//  MazenProject
//
//  Created by Mazen Denden on 10/11/19.
//  Copyright © 2019 Mazen Denden. All rights reserved.
//

import UIKit

let homeTableViewCellId = "homeTableViewCellId"

class HomeViewController: BaseViewController {
    
    let moviesTableView: UITableView = {
        let tableView = UITableView()
        tableView.backgroundColor = ColorConstants.backgroundBlack
        tableView.separatorStyle = .none
        return tableView
    }()
    
    let homeViewModel = HomeViewModel()
    
    // appDelegate instance
    weak var appDelegate = UIApplication.shared.delegate as? AppDelegate
    
    let refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.tintColor = ColorConstants.backgroundRed
        refreshControl.addTarget(self, action: #selector(refreshAction), for: .valueChanged)
        return refreshControl
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        moviesTableView.delegate = self
        moviesTableView.dataSource = self
        moviesTableView.register(HomeTableViewCell.self, forCellReuseIdentifier: homeTableViewCellId)
        
        homeViewModel.delegate = self
        appDelegate?.lifeCycleDelegate = self
        
        setupNavigationBar()
        setupViews()
    }
    
    @objc func refreshAction() {
        if !moviesTableView.isDragging {
            self.homeViewModel.getMovies()
        }
    }
    
    func setupNavigationBar() {
        self.navigationItem.leftBarButtonItem = nil
        self.navigationItem.hidesBackButton = true
        
        self.navigationItem.titleView = UIImageView(image: #imageLiteral(resourceName: "nLogo"))

    }
    
}
