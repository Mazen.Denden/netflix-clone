//
//  HomeViewController+Extensions.swift
//  MazenProject
//
//  Created by Mazen Denden on 8/4/20.
//  Copyright © 2020 Mazen Denden. All rights reserved.
//

import UIKit

extension HomeViewController {
    func setupViews() {
        
        moviesTableView.refreshControl = refreshControl
        
        moviesTableView.tableFooterView = UIView()
        
        view.addSubview(moviesTableView)
        moviesTableView.anchor(topAnchor: (view.safeAreaTopAnchor, 12),
                               leftAnchor: (view.safeAreaLeftAnchor, 12),
                               rightAnchor: (view.safeAreaRightAnchor, 12),
                               bottomAnchor: (view.safeAreaBottomAnchor, 12))
        
        self.homeViewModel.getMovies()
    }
    
    func didSelectMovie(movieViewModel: MovieViewModel) {
        let movieDetailViewController = MovieDetailsViewController()
        movieDetailViewController.movieViewModel = movieViewModel
        self.navigationController?.pushViewController(movieDetailViewController, animated: true)
    }
}

extension HomeViewController: HomeViewModelDelegate {
    func setEvent(event: HomeViewModelEventType) {
        switch event {
        case .reloadTableView:
            DispatchQueue.main.async {
                self.moviesTableView.reloadData()
            }
            
        case .showLoader:
            DispatchQueue.main.async {
                if !self.refreshControl.isRefreshing {
                    Loader.showLoader()
                }
            }
            
        case .hideLoader:
            DispatchQueue.main.async {
                Loader.hideLoader()
            }
            
        case .endRefresh:
            DispatchQueue.main.async {
                self.refreshControl.endRefreshing()
            }
        }
    }
    
    func presentAlert(message: String) {
        self.showSimpleAlert(message: message)
    }
}

extension HomeViewController: AppLifeCycleDelegate {
    func handleAppWillEnterForeground() {
        print(#function)
    }
    
    func handleAppDidBecomeActive() {
        print(#function)
    }
    
    func handleAppWillResignActive() {
        print(#function)
    }
    
    func handleAppDidEnterBackground() {
        print(#function)
    }
}
