//
//  HomeViewController+TableView.swift
//  MazenProject
//
//  Created by Mazen Denden on 10/11/19.
//  Copyright © 2019 Mazen Denden. All rights reserved.
//

import UIKit

private let headerSectionHeight: CGFloat = 112
private let sectionHeight: CGFloat = 140
private let headerHeight = UIScreen.main.bounds.height * 0.055

extension HomeViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return homeViewModel.sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return homeViewModel.teasersArray.count
            
        case 1:
            return homeViewModel.featuredArray.count
            
        default:
            return homeViewModel.othersArray.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: homeTableViewCellId, for: indexPath) as? HomeTableViewCell else { return UITableViewCell() }
        
        var moviesViewModelArray: [MovieViewModel] = []
        
        switch indexPath.section {
        case 0:
            cell.imageType = .circle
            moviesViewModelArray = homeViewModel.teasersArray[indexPath.row].movies.map { return MovieViewModel(movie: $0) }
            
        case 1:
            cell.imageType = .rectangle
            moviesViewModelArray = homeViewModel.featuredArray[indexPath.row].movies.map { return MovieViewModel(movie: $0) }
            
        default:
            cell.imageType = .rectangle
            moviesViewModelArray = homeViewModel.othersArray[indexPath.row].movies.map { return MovieViewModel(movie: $0) }
            
        }
        
        cell.moviesViewModels = moviesViewModelArray
        cell.didSelectMovie = self.didSelectMovie
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return headerSectionHeight
        default:
            return sectionHeight
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return homeViewModel.sections[section].type?.rawValue.localized()
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return headerHeight
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header: HomeHeaderView = {
            let view = HomeHeaderView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: headerHeight))
            view.backgroundColor = ColorConstants.backgroundBlack
            return view
        }()
        let headerTitle = self.tableView(tableView, titleForHeaderInSection: section) ?? ""
        header.setTitle(title: headerTitle)
        
        return header
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if refreshControl.isRefreshing {
            homeViewModel.getMovies()
        }
    }
    
}
