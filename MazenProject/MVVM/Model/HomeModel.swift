//
//  Section.swift
//  MazenProject
//
//  Created by Mazen Denden on 10/16/19.
//  Copyright © 2019 Mazen Denden. All rights reserved.
//

import Foundation

struct HomeModel: Codable {
    var movies: [Movie] = []
    var type: SectionType?
    
    private enum CodingKeys: String, CodingKey {
        case type
        case movies = "data"
    }
}
