//
//  AppDelegate+LifeCycle.swift
//  MazenProject
//
//  Created by Mazen Denden on 11/18/19.
//  Copyright © 2019 Mazen Denden. All rights reserved.
//

import UIKit

protocol AppLifeCycleDelegate: class {
    
    // background <-> inactive <-> active
    
    /* Called:
     * from background to inactive
     */
    func handleAppWillEnterForeground()
    
    /* Called:
     * on launch
     * from inactive to active
    */
    func handleAppDidBecomeActive()
    
    /* Called:
     * before switching apps
     * from active to inactive
     */
    func handleAppWillResignActive()
    
    /* Called:
     * after switching apps
     * clicking on the home button ( from inactive to background )
     */
    func handleAppDidEnterBackground()
}

// Default Implementation to make the Protocol Optionnal
extension AppLifeCycleDelegate {
    func handleAppWillEnterForeground() {}
    func handleAppDidBecomeActive() {}
    func handleAppWillResignActive() {}
    func handleAppDidEnterBackground() {}
}

extension AppDelegate {
    func applicationWillEnterForeground(_ application: UIApplication) {
        lifeCycleDelegate?.handleAppWillEnterForeground()
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        lifeCycleDelegate?.handleAppDidBecomeActive()
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        lifeCycleDelegate?.handleAppWillResignActive()
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        lifeCycleDelegate?.handleAppDidEnterBackground()
    }
}
