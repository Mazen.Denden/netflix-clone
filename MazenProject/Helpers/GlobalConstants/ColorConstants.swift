//
//  ColorConstants.swift
//  MazenProject
//
//  Created by Mazen Denden on 10/10/19.
//  Copyright © 2019 Mazen Denden. All rights reserved.
//

import UIKit

struct ColorConstants {
    static let backgroundRed = UIColor.rgb(red: 234, green: 64, blue: 48, alpha: 1)
    static let backgroundBlack = UIColor.rgb(red: 24, green: 24, blue: 24, alpha: 1)
    static let transparentBlackColor = UIColor.rgb(red: 0, green: 0, blue: 0, alpha: 0.35)
}
