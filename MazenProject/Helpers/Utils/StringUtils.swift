//
//  StringUtils.swift
//  MazenProject
//
//  Created by Mazen Denden on 11/13/19.
//  Copyright © 2019 Mazen Denden. All rights reserved.
//

import Foundation

struct StringUtils {
    
    private init() {}
    
    static func className(_ obj: AnyObject) -> String {
        return String(describing: type(of: obj))
    }
}
