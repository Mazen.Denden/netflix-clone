//
//  SectionType.swift
//  MazenProject
//
//  Created by Mazen Denden on 10/17/19.
//  Copyright © 2019 Mazen Denden. All rights reserved.
//

import Foundation

enum SectionType: String, Codable {
    case teasers = "Teasers"
    case featured = "Featured"
    case other = "Other"
}
