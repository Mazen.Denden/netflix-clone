//
//  HomeViewModelEventType.swift
//  MazenProject
//
//  Created by Mazen Denden on 10/23/19.
//  Copyright © 2019 Mazen Denden. All rights reserved.
//

import Foundation

enum HomeViewModelEventType {
    case reloadTableView
    case showLoader
    case hideLoader
    case endRefresh
}
