//
//  LabelExtension.swift
//  MazenProject
//
//  Created by Mazen Denden on 9/23/19.
//  Copyright © 2019 Mazen Denden. All rights reserved.
//

import UIKit

extension UILabel {
    @IBInspectable var localizedKey: String? {
        get {
            return self.text ?? ""
        }
        set {
            self.text = newValue?.localized()
        }
    }
    
    func shakeAnimation() {
        let shakeAnimation = CABasicAnimation(keyPath: "position")
        shakeAnimation.duration = 0.05
        shakeAnimation.repeatCount = 2
        shakeAnimation.autoreverses = true
        
        shakeAnimation.fromValue = NSValue(cgPoint: CGPoint(x: self.center.x - 5, y: self.center.y))
        shakeAnimation.toValue = NSValue(cgPoint: CGPoint(x: self.center.x + 5, y: self.center.y))
        
        self.layer.add(shakeAnimation, forKey: "position")
    }
}
