//
//  MazenProjectTests.swift
//  MazenProjectTests
//
//  Created by Mazen Denden on 8/14/19.
//  Copyright © 2019 Mazen Denden. All rights reserved.
//

import XCTest
@testable import MazenProject

class MazenProjectTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    
    func testServiceGetMovies() {
        guard let url = URL(string: Webservice.shared.url) else { return }
        
        let expectation = self.expectation(description: "Status code: 200")
        
        let urlSession = URLSession.shared
        let dataTask = urlSession.dataTask(with: url) { data, response, error in
            
            if let error = error {
                XCTFail("Error: \(error.localizedDescription)")
                return
            }
            
            if let statusCode = ( response as? HTTPURLResponse )?.statusCode {
                if statusCode == 200 {
                    expectation.fulfill()
                } else {
                    XCTFail("Status code: \(statusCode)")
                }
            }
            
            if let data = data {
                do {
                    let moviesDecoded = try JSONDecoder().decode([HomeModel].self, from: data as Data)
                    XCTAssertNotNil(moviesDecoded)
                    
                } catch let error {
                    XCTFail(error.localizedDescription)
                }
            }
        }
        
        dataTask.resume()
        wait(for: [expectation], timeout: 10)
    }
}
